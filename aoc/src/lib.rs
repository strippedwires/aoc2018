
pub mod aoc {

    use std::fs::File;
    use std::io::prelude::*;
    use std::io;

    pub struct AOCInput {
        pub contents: String
    }

    impl AOCInput {

        pub fn new(filename: &str) -> io::Result<AOCInput> {

            let mut f = File::open(filename)?;

            let mut contents = String::new();

            let buf = f.read_to_string(&mut contents)?;

            Ok(
                AOCInput {
                    contents: contents 
                }
            )

        }
    }

}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
