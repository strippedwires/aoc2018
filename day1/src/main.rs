use std::env;
use std::io::prelude::*;
use std::fs::File;
use std::collections::HashSet;

struct Device {
    freq: i32,
    history: HashSet<i32>
}

impl Device {

    fn process(&self, buf: &String) -> i32 {
        buf
            .lines()
            .map(|s| s.parse::<i32>().unwrap())
            .fold(self.freq, |acc, x| acc + x)
    }

    fn find_first_repeat(&mut self, buf: &String) -> i32 {

        let mut seq =  buf.lines()
            .map(|s| s.parse::<i32>().unwrap())
            .cycle();

        loop {
            self.freq = self.freq + seq.next().unwrap();
            if self.history.contains(&self.freq) {
                break
            }
            self.history.insert(self.freq);
        }

        self.freq
    }
}

fn part_one(buf: &String) -> i32 {

    let dev = Device { freq: 0, history: HashSet::new() };
    
    dev.process(buf)

}

fn part_two(buf: &String) -> i32 {
    let mut dev = Device { freq: 0, history: HashSet::new() };

    dev.find_first_repeat(buf)
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let mut f = File::open(filename).expect("Couldn't open file");

    let mut contents = String::new();
    
    f.read_to_string(&mut contents)
        .expect("Couldn't read file");

    println!("Part One result is: {}", part_one(&contents));

    println!("Part Two result is: {}", part_two(&contents));
}