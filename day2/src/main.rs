extern crate aoc;

use aoc::aoc::AOCInput;
use std::collections::HashMap;
use std::env;

fn twice_thrice(item: &str) -> (bool, bool) {
    let mut count: HashMap<char, u8> = HashMap::new();

    for chr in item.chars() {
        let counter = count.entry(chr).or_insert(0);
        *counter += 1;
    }

    let twice = match count.values().find(|&&v| v == 2) {
        Some(_) => true,
        None => false,
    };

    let thrice = match count.values().find(|&&v| v == 3) {
        Some(_) => true,
        None => false,
    };

    (twice, thrice)
}

fn checksum(buf: &String) -> u32 {
    let counts = buf
        .lines()
        .map(|l| twice_thrice(l))
        .fold((0, 0), |acc, result| match result {
            (true, true) => (acc.0 + 1, acc.1 + 1),
            (true, false) => (acc.0 + 1, acc.1),
            (false, true) => (acc.0, acc.1 + 1),
            (false, false) => acc,
        });
    counts.0 * counts.1
}

fn compare_ids(left: &str, right: &str) -> Option<usize> {
    let diffs = left
        .chars()
        .zip(right.chars())
        .enumerate()
        .filter(|(_p, (l, r))| l != r)
        .collect::<Vec<(usize, (char, char))>>();

    if diffs.len() == 1 {
        Some(diffs[0].0)
    } else {
        None
    }
}

fn find_common(buf: &String) -> String {

    let right = buf.clone();
    
    for l in buf.lines() {
        for r in right.lines() {
            if let Some(pos) = compare_ids(l, r) {
                let mut res = l.to_string();
                res.remove(pos);
                return res;
            }
        }
    }

    "Boo".to_string()
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let input = AOCInput::new(filename).unwrap();

    println!("Checksum: {:?}", checksum(&input.contents));
    println!("Matches: {:?}", find_common(&input.contents));
}
